function createList(arr, container = document.body) {

    let ul = document.createElement("ul");
    container.append(ul);
    createListElements(arr, ul);

    return ul;
}

function createListElements(arr, ul) {

    for (const element of arr) {

        if (Array.isArray(element)) {

            let innerUl = document.createElement("ul");
            ul.append(innerUl);  
            createListElements(element, innerUl);
        } else {

            let listElement = document.createElement("li");
            listElement.innerHTML = element;
            ul.append(listElement);  
        }
    }
}

function countdownRemove(seconds, element, parent = document.body) {

    setTimeout(() => element.remove(), (seconds * 1000));

    let timerBlock = document.createElement("div");
    timerBlock.innerHTML = seconds;

    let tick = setInterval(function() {
        timerBlock.innerHTML = (seconds -= 1)
        
        if (timerBlock.innerHTML == 0) {
            clearInterval(tick);
            timerBlock.remove();

        }
    }, 1000);

    parent.append(timerBlock);

}

let myArr = ["Kharkiv", "Kiev", ["Borispol", ["Borispol", "Irpin"], "Irpin"], "Odessa", "Lviv", "Dnieper", ["Borispol", "Irpin"]];
let list = createList(myArr);
countdownRemove(3, list);


// Sample arrays for testing:
// ["Kharkiv", "Kiev", "Borispol"]
// ["Kharkiv", "Kiev", ["Borispol", ["Borispol", "Irpin"], "Irpin"], "Odessa", "Lviv", "Dnieper", ["Borispol", "Irpin"]]